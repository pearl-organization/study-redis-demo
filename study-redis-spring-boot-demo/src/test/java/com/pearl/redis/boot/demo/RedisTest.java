package com.pearl.redis.boot.demo;

import com.pearl.redis.boot.demo.cache.User;
import com.pearl.redis.boot.demo.config.SysConfig;
import com.pearl.redis.boot.demo.config.SysConfigMapper;
import com.pearl.redis.boot.demo.repo.PPP;
import com.pearl.redis.boot.demo.repo.Person;
import com.pearl.redis.boot.demo.repo.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;


@SpringBootTest
public class RedisTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    @Test
    public void test() {
        // 增
        User user = new User("张三", 18);
        redisTemplate.boundHashOps("session").put("id:8989", user);

        // 查
        User getUser = (User) redisTemplate.boundHashOps("session").get("id:8989");
        System.out.println(getUser);
    }

    @Autowired
    PersonRepository repo;

    @Autowired
    PPP ppp;


    @Test
    public void basicCrudOperations() {

        Person rand = new Person("rand", "al'thor");

        Person save = repo.save(rand);

        long count = repo.count();

        Pageable pageable = PageRequest.of(1, 10);
        Page<Person> all = ppp.findAll(pageable);
        System.out.println(all);

        //repo.delete(rand);

        stringRedisTemplate.opsForValue().setIfAbsent("key", "value", 1000, TimeUnit.SECONDS);
        stringRedisTemplate.delete("key");
    }


    @Autowired
    private SysConfigMapper sysConfigMapper;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        SysConfig sysConfig = sysConfigMapper.selectConfigById(111L);
        System.out.println(sysConfig);
    }

}
