package com.pearl.redis.boot.demo.cache;

import java.io.Serializable;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/30
 */
public class User implements Serializable {

    private String name;

    private Integer age;

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
