package com.pearl.redis.boot.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
@EnableRedisRepositories
@MapperScan("com.pearl.redis.boot.demo.config")
public class StudySpringBootDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudySpringBootDemoApplication.class, args);
    }

}
