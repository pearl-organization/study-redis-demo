package com.pearl.redis.boot.demo.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/30
 */

@RestController
@RequestMapping("/cache")
public class CacheController {


    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    private static Integer dbStockCount = 100; // 商品库存数量

    @GetMapping("/getStockCount")
    public Object getStockCount() {
        // 1. 缓存中查询
        if (Boolean.TRUE.equals(redisTemplate.hasKey("stockCount"))) {
            // 2. 缓存中有 直接返回
            Integer cacheCount = (Integer) redisTemplate.opsForValue().get("stockCount");
            return dbStockCount + ":" + cacheCount;
        } else {
            // 3. 缓存中没有，数据库查询，设置缓存库存数量，并返回
            Integer curStockCount = dbStockCount;
            redisTemplate.opsForValue().set("stockCount", curStockCount);
            return dbStockCount + ":" + curStockCount;
        }


        // 双检加锁

/*        // 1. 第一次查询缓存
        Object value = redisTemplate.opsForValue().get("key001");
        if (value != null) {
            // 缓存命中
            return value;
        } else {
            // 2. 缓存未命中 添加互斥锁
            synchronized (this) {
                // 3. 第二次查询缓存
                value = redisTemplate.opsForValue().get("key001");
                if (value != null) {
                    // 缓存命中 直接返回
                    return value;
                } else {
                    // 再次 缓存未命中，查询数据库并加载缓存中
                    value = "value"; // 模拟查询数据库
                    redisTemplate.opsForValue().set("key001", "value");
                    return value;
                }
            }
        }*/
    }


    @GetMapping("/update")
    public void update() throws InterruptedException {
        // 1. 删除缓存
        // 2. 执行业务逻辑.........

        // 3. 先更新数据库
        dbStockCount = dbStockCount - 1;

        // 4. 延迟一段时间
        TimeUnit.MILLISECONDS.sleep(600);

        // 5. 再删除缓存
        Boolean result = redisTemplate.delete("stockCount");

    }


    @GetMapping("/insertOrder")
    public Object insertOrder() {
        // 1. 在Redis 存入一个K-V，当设置失败时，说明有别的线程在操作，返回“当前人数过多，请稍后重试”
        String key = "lock_key";
        Boolean result = redisTemplate.opsForValue().setIfAbsent(key, 1, 5L, TimeUnit.SECONDS);
        // 2. 如果设置失败，则直接返回
        if (!result) {
            return "当前人数过多，请稍后重试";
        }
        try {
            // 设置成功K ，才可以进行下单操作
            // 查询库存量
            Integer stock = (Integer) redisTemplate.opsForValue().get("stock");
            if (stock > 0) {
                // 如果库存 > 0， 库存 -1
                Integer num = stock - 1;
                redisTemplate.opsForValue().set("stock", num);
                System.out.println("下单成功，当前剩余库存量：" + num);
                return "下单成功";
            } else {
                System.out.println("下单失败，库存已被秒空");
                return "下单失败";
            }
        } finally {
            // 3. 操作完成后，删除KEY
            redisTemplate.delete(key);
        }
    }


    @Autowired
    StringRedisTemplate stringRedisTemplate;

    private static final String LOCK_KEY = "RedisLock:study-redis-spring-boot-demo:CacheController:sale";

    @GetMapping("/sale")
    public String sale() {
        // 1. 尝试获取锁
        String uuid = UUID.randomUUID().toString() + Thread.currentThread().getId();
        Boolean result = stringRedisTemplate.opsForValue().setIfAbsent(LOCK_KEY, uuid, 30L, TimeUnit.SECONDS);
        // 2. 如果设置失败，则直接返回
        if (Boolean.FALSE.equals(result)) {
            return "当前人数过多，请稍后重试";
        }
        try {
            // 3. 获取到锁，执行扣减操作
            String stock = stringRedisTemplate.opsForValue().get("stock");// 查询库存量
            System.out.println("当前剩余库存量：" + stock);
            if (stock != null && Integer.parseInt(stock) > 0) { // 如果库存 > 0
                Integer num = Integer.parseInt(stock) - 1; //  库存 -1
                stringRedisTemplate.opsForValue().set("stock", String.valueOf(num)); // 设置新库存
                return "下单成功";
            } else {
                return "下单失败，库存已被秒空";
            }
        } finally {
            // 4. 操作完成后，删除KEY
            if (Objects.requireNonNull(stringRedisTemplate.opsForValue().get(LOCK_KEY)).equalsIgnoreCase(uuid)) {
                stringRedisTemplate.delete(LOCK_KEY);
            }
        }
    }


    @GetMapping("/aaa")
    public String AAA() {
        // 1. 尝试获取锁
        //
        Boolean result = stringRedisTemplate.opsForValue().setIfAbsent(LOCK_KEY, "uuid", 30L, TimeUnit.SECONDS);
        // 2. 递归
        AAA();
        return "";
    }
}
