package com.pearl.redis.boot.demo.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/30
 */
public interface PPP extends PagingAndSortingRepository<Person, String> {
}
