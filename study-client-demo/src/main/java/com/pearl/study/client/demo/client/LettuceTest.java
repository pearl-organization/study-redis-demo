package com.pearl.study.client.demo.client;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author TD
 * @version 1.0
 * @date 2024/6/6
 */
public class LettuceTest {

    public static void main(String[] args) {
        // 创建客户端
        RedisClient redisClient = RedisClient.create("redis://localhost:6379");
        // 获取连接
        try (StatefulRedisConnection<String, String> connection = redisClient.connect()) {
            // 执行异步命令
            RedisAsyncCommands<String, String> asyncCommands = connection.async();

            // 存入一个字符串
            asyncCommands.set("foo", "bar").get();
            System.out.println(asyncCommands.get("foo").get()); // prints bar

            // 存入一个Hash
            Map<String, String> hash = new HashMap<>();
            hash.put("name", "John");
            hash.put("surname", "Smith");
            hash.put("company", "Redis");
            hash.put("age", "29");
            asyncCommands.hset("user-session:123", hash).get();

            System.out.println(asyncCommands.hgetall("user-session:123").get()); // Prints: {name=John, surname=Smith, company=Redis, age=29}
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            redisClient.shutdown();
        }
    }
}
