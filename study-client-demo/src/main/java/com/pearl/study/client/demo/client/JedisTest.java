package com.pearl.study.client.demo.client;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2024/6/6
 */
public class JedisTest {
    public static void main(String[] args) {
        // 创建连接池
        JedisPool pool = new JedisPool("localhost", 6379,"default","123456");
        // 获取客户端
        try (Jedis jedis = pool.getResource()) {
            // 存入一个字符串
            jedis.set("foo", "bar");
            System.out.println(jedis.get("foo"));   //Prints: bar

            // 存入一个Hash
            Map<String, String> hash = new HashMap<>();;
            hash.put("name", "John");
            hash.put("surname", "Smith");
            hash.put("company", "Redis");
            hash.put("age", "29");
            jedis.hset("user-session:123", hash);

            jedis.del("user-session:123")

            System.out.println(jedis.hgetAll("user-session:123"));  // Prints: {name=John, surname=Smith, company=Redis, age=29}
        }
    }
}
