package com.pearl.study.client.demo.lua;

import io.lettuce.core.RedisClient;
import io.lettuce.core.ScriptOutputType;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/25
 */
public class FunTest {

    public static void main(String[] args) {
        // 创建客户端
        RedisClient redisClient = RedisClient.create("redis://:123456@127.0.0.1:6379/0");
        // 获取连接
        try (StatefulRedisConnection<String, String> connection = redisClient.connect()) {
            // 执行命令
            RedisCommands<String, String> sync = connection.sync();
/*            String msg = sync.functionLoad("#!lua name=mylib\nredis.register_function('knockknock', function() return 'Who\\'s there?' end)");
            System.out.println(msg);*/

            String res = sync.fcall("knockknock", ScriptOutputType.VALUE);
            System.out.println(res);

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            redisClient.shutdown();
        }
    }


}
