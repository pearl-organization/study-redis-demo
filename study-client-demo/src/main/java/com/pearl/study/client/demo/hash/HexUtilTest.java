package com.pearl.study.client.demo.hash;

import cn.hutool.core.io.checksum.CRC16;
import cn.hutool.core.util.HexUtil;
import io.lettuce.core.cluster.SlotHash;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/11
 */
public class HexUtilTest {

    public static void main(String[] args) {
        // lettuce
        int key001 = SlotHash.getSlot("key001");
        int key002 = SlotHash.getSlot("key002");
        System.out.println(key001); // 12657
        System.out.println(key002); // 274
    }
}
