package com.pearl.study.client.demo.bf;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.sync.RedisCommands;
import redis.clients.jedis.UnifiedJedis;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/23
 */
public class BloomFilterExample {

    public static void main(String[] args) {
        UnifiedJedis jedis = new UnifiedJedis("redis://192.168.56.101:6379");

        // 创建
        String res1 = jedis.bfReserve("bikes:models", 0.01, 1000);
        System.out.println(res1); // >>> OK

        // 添加
        boolean res2 = jedis.bfAdd("bikes:models", "Smoky Mountain Striker");
        System.out.println(res2); // >>> True

        // 判断是否存在
        boolean res3 = jedis.bfExists("bikes:models", "Smoky Mountain Striker");
        System.out.println(res3); // >>> True
    }
}
