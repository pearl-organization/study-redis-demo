package com.pearl.study.client.demo.client;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

import java.util.List;

/**
 * @author TD
 * @version 1.0
 * @date 2024/7/5
 */
public class PipelineTest {

    public static void main(String[] args) {
        // 创建连接池
        JedisPool pool = new JedisPool("localhost", 6379,"default","123456");
        // 获取客户端
        try (Jedis jedis = pool.getResource()) {

            // 1. 不使用管道， 插入 10000 个 Key
            long startTime = System.currentTimeMillis();
            for (int i = 10000; i > 0; i--) {
                //jedis.set("key" + i, String.valueOf(i));
                jedis.get("key" + i);
            }
            System.out.println("不使用管道执行时间：" + ( System.currentTimeMillis()-startTime)+"毫秒");

            // 2. 使用管道
            startTime = System.currentTimeMillis();
            Pipeline pipelined = jedis.pipelined();
            for (int i = 10000; i > 0; i--) {
                //pipelined .set("key" + i, String.valueOf(i));
                pipelined.get("key" + i);
            }
            // 获取执行结果
            List<Object> results = pipelined.syncAndReturnAll();
            System.out.println("管道执行时间：" + ( System.currentTimeMillis()-startTime)+"毫秒");
        }
    }
}
