package com.pearl.study.client.demo.client;

import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author TD
 * @version 1.0
 * @date 2024/6/6
 */
public class RedissonTest {

    public static void main(String[] args) {
        // 创建客户端对象
        Config config = new Config();
        config.useSingleServer().setAddress("redis://127.0.0.1:6379");
        RedissonClient redissonClient = Redisson.create(config);

        // 存入一个 Map
        RMap<String, String> rMap = redissonClient.getMap("rMap"); // 创建Map分布式对象
        rMap.put("name", "John"); // 存入数据
        rMap.put("surname", "Smith");
        rMap.put("company", "Redis");

        // 重新获取
        RMap<String, String> getMap = redissonClient.getMap("rMap");
        System.out.println(getMap);
    }
}
