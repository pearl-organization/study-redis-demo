#!lua name=mylib

local function getV(keys)
  local key = keys[1]
  return redis.call('GET', key)
end