local ipKey = KEYS[1]; -- IP地址
local rate = tonumber(ARGV[1]); -- 允许的请求次数
local requestCount = redis.call('incr', ipKey); -- 每次请求 + 1
if (requestCount == 1) then -- 第一次请求
    redis.call('expire', ipKey, 60); -- 设置过期时间(60S)
    return true ; -- 返回是否允许访问
else -- 不是第一次请求
    if (requestCount > rate) then -- 如果当前请求次数大于允许的请求次数
         return false ;
    end
end

